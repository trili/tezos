meta:
  id: id_019__ptparisa__vote__ballots
  endian: be
doc: ! 'Encoding id: 019-PtParisA.vote.ballots'
seq:
- id: yay
  type: s8be
- id: nay
  type: s8be
- id: pass
  type: s8be
